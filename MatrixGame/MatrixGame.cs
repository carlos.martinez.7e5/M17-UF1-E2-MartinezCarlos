﻿using System;
using GameTools;
using matrixFinal;

namespace MatrixGame
{
    class MatrixGame : GameEngine
    {
        MatrixRepresentation main; //La matriz "principal" que se imprime
        MatrixRepresentation aux; //Una matriz auxiliar. Usada temporalmente para bajar los carácteres
        PrintColorines pC; //Clase para poder imprimir la matriz con diferentes colores
        Random rnd; //Random para generar aleatoriamente cada carácter y la columna por donde caerá
        
        char[] letras; //Array con todos los carácteres
        int letra; //Numero que indica que posición del array de caracteres (que letra) caerá
        int charColumna; //La columna por donde caerá el carácter

        protected override void Start()
        {
            //Al inicio del programa creo dos matrices y las limpio (pongo todo a 0)
            main = new MatrixRepresentation(25, 45); //25 , 45 
            aux = new MatrixRepresentation(main.TheMatrix.GetLength(0), main.TheMatrix.GetLength(1));

            main.CleanTheMatrix();
            aux.CleanTheMatrix();
        }

        protected override void Update()
        {
            letras = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            pC = new PrintColorines();
            rnd = new Random();

            //Función que indica cuanas letras caerán en cada fotograma
            letrasCaen(6,rnd);

            //Recorre la matriz entera
            for (int fila = 0; fila < main.TheMatrix.GetLength(0); fila++)
            {
                for (int columna = 0; columna < main.TheMatrix.GetLength(1); columna++){

                    //En caso de que el carácter sea distinto de 0...
                    if(main.TheMatrix[fila,columna] != '0')
                    {
                        //Si la fila no es la última, se guarda el carácter una posición más abajo en la matriz auxiliar.
                        if (fila != main.TheMatrix.GetLength(0) - 1)
                        {
                            /*Por si quieres que las letras cambien cada frame
                            aux.TheMatrix[fila + 1, columna] = letras[rnd.Next(0, letras.Length - 1)]; */
                            
                            aux.TheMatrix[fila + 1, columna] = main.TheMatrix[fila, columna];
                            if (aux.TheMatrix[fila, columna] != '0') aux.TheMatrix[fila, columna] = '0';
                        }
                        //Si es la última fila, se pone a 0
                        else aux.TheMatrix[fila, columna] = '0';
                    }
                }
            }

            //Se copia la matriz auxiliar (la de los carácteres movidos) a la principal
            main.clippingMatrix(aux.TheMatrix);
            
            //Se imprime por pantalla
            pC.printMatrix(main.TheMatrix);
        }

        private void letrasCaen(int cantidad, Random rnd)
        {
            for(int i = 0; i < cantidad; i++)
            {
                charColumna = rnd.Next(0, main.TheMatrix.GetLength(1));
                letra = rnd.Next(0, letras.Length - 1);
                main.TheMatrix[0, charColumna] = letras[letra];
            }
        }

        protected override void Exit()
        {
            base.Exit();
        }
    }
}
