﻿using System;
namespace GameTools
{
    public class GameEngine
    {
        private ConsoleColor _backgroundConsoleColor;
       
        public ConsoleColor BackgroundConsoleColor
        {
            get { return _backgroundConsoleColor; }
            set
            {
                if (ConsoleColor.White != value || ConsoleColor.Black != value) _backgroundConsoleColor = value;
                else
                {
                    _backgroundConsoleColor = ConsoleColor.Gray;
                    throw new ArgumentException($"Console color {value} not recomended. Set by default");
                }
            }
        }

        private int time2liveframe;
        private float _frameRate;
        public float FrameRate
        {
            get { return _frameRate; }
            set
            {
                _frameRate = (value < 0f) ? value * (-1f) : value;
            }
        }

        public int Frames { get; set; }

        private ConsoleKeyInfo cki;
        private bool engineSwitch;

        public GameEngine()
        {
            InitGame();
            UpdateGame();
            CloseGame();
        }

        private void InitGame()
        {
            Frames = 0;
            engineSwitch = false;

            this._frameRate = (_frameRate <= 0) ? 12 : _frameRate;
           
            time2liveframe = (int)((1 / _frameRate) * 1000);

            CleanFrame();
            Console.BackgroundColor = _backgroundConsoleColor;

            Console.WriteLine("\nPress a key to display; press the ESC key to quit.");

            Console.WriteLine($"Game Initiation             Render data: Framerate: {_frameRate} || TimeToRefresh:{time2liveframe}");

            Start();

            System.Threading.Thread.Sleep(2000);
        }

        private void UpdateGame()
        {
            do
            {
                while (Console.KeyAvailable == false)
                {
                 
                    CleanFrame();
                    Update();
                    CheckKeyboard4Engine();
                    RefreshFrame();
                    Frames++;
                }
                cki = Console.ReadKey(true);
            } while (engineSwitch);
        }

        private void ListenKeyboard()
        {
            cki = Console.ReadKey();
        }

        private void CheckKeyboard4Engine()
        {
            engineSwitch = (cki.Key == ConsoleKey.Escape);
        }

        private void RefreshFrame()
        {
            //Access to Threading library only in this line
            System.Threading.Thread.Sleep(time2liveframe);
        }

        private void CloseGame()
        {
            Console.WriteLine("You pressed the '{0}' key.", cki.Key);
            Exit();
            Console.WriteLine(" Game Over. Closing game");
        }


        private void CleanFrame()
        {
            Console.Clear();
            Console.SetCursorPosition(Console.CursorLeft, Console.CursorTop+1);
        }

        protected virtual void Start()
        {
            //Code before first frame
        }

        protected virtual void Update()
        {
            //Execution ontime secuence of every frame
        }

        protected virtual void Exit()
        {
            //Code afer last frame
        }
    }
}


public class ConsoleSpiner
{
    int counter;
    public ConsoleSpiner()
    {
        counter = 0;
    }
    public void Turn()
    {
        counter++;
        switch (counter % 4)
        {
            case 0: Console.Write("/"); break;
            case 1: Console.Write("-"); break;
            case 2: Console.Write("\\"); break;
            case 3: Console.Write("|"); break;
        }
        //Console.SetCursorPosition(Console.CursorLeft - 1, Console.CursorTop);
    }
}
